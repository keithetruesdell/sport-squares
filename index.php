<?php
	// define the site path
	$site_path = realpath(dirname(__FILE__));
	define ("__SITE_PATH", $site_path);
	
	date_default_timezone_set ("America/New_York");
	
	// get config options 
	include_once(__SITE_PATH."/inc/config.php");

	// get header options and contents 
	include_once(__SITE_PATH."/inc/headers.php");

	// get square contents 
	include_once(__SITE_PATH."/inc/squares.php");
	
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta content="en-us" http-equiv="Content-Language" />
		<meta content="text/html; charset=windows-1252" http-equiv="Content-Type" />
		<meta name="title" content="Football Squares" />
		<meta name="Author" content="" />
		<meta name="Subject" content="Football Squares" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<meta name="Language" content="English" />
		<meta name="Designer" content="" />
		<meta name="Publisher" content="" />
		<meta name="Revisit-After" content="1 days Days" />
		<meta name="Distribution" content="Global">
		<meta name="Robots" content="all" />
		<base target="_self" />
		
		<title>Football Squares</title>
		
		<!-- Shortcut Icon -->
		<link rel="icon" type="image/x-icon" href="/favicon.ico" />
		<link rel="shortcut icon" href="/favicon.ico" />
		<link rel="apple-touch-icon" href="/apple-touch-icon.png" />
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="/assets/css/bootstrap/4.5.0/bootstrap.min.css" crossorigin="anonymous">
		
		<!-- Font Awesome Free -->
		<link rel="stylesheet" href="/assets/css/fontawesome/5.13/all.min.css" />
		
		<!-- Google fonts -->
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
		<link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
		<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
		
		
		<!-- load theme css files -->
		<link rel="stylesheet" href="/assets/css/bootstrap/custom.css" />
		<style>
			main, #main, .main { margin: 15px; }
			.squares-container { border-width: .2rem; border: solid #000000; }
			.row { margin-right: 0px; margin-left: 0px; }
			.row>.col { border: 1px solid #000000; padding-top: 1rem; padding-bottom: 1rem; }
		
			.bg-grey { background-color: #c7c7c7; }
			
			.bolder { font-weight: bolder; }
			.ucase, .upper { text-transform: capitalize; }
			
			.btnClick>a { cursor: pointer; }
		</style>
	</head>
	<body>
		<main>
			
			<div class="squares-container text-center">
			
			<?php 
				
				// create a row of column headers
				// first - create blank cell
				echo "<div class='row'>";
				echo "<div class='col'></div>";
				foreach ($colHeaders AS $colName) {
					echo "<div class='col bg-grey bolder upper'>";
					echo "<span class=''>".trim($colName)."</span>";
					echo "</div>";
				}
				echo "</div>";
				
				// create grid of squares
				$squaresData = $squares["data"];
				foreach ($squaresData AS $rowIdx=>$squaresRow) {
					echo "<div class='row'>";
					// add row headers
					echo "<div class='col bg-grey bolder upper'>".trim($rowHeaders[$rowIdx])."</div>";
					
					// add row squares with data 
					foreach ($squaresRow AS $colIdx=>$square) {
						$squareID = $colIdx.$rowIdx;
						echo "<div class='col btnClick'>";
						
						echo STRLEN(TRIM($square['displayName'])) > 1 ? "<span class='bg-grey' data-sqid='".$squareID."'>".$square['displayName']."</span>" : "<a class='btn-opensquare' data-colid='".$colHeaders[$colIdx]."' data-rowid='".$rowHeaders[$rowIdx]."' data-square='".$colHeaders[$colIdx].$rowHeaders[$rowIdx]."' data-sqid='".$squareID."' onClick='openSquareClick(this)' ><span class=''>OPEN</span></a>";
						
						echo "</div>";
					}
					echo "</div>";
				}
			?>
			</div>
		</main>
		
		<script>
			var btnOpenSq = document.getElementsByClassName("btn-opensquare");
			console.log(btnOpenSq);
			
			document.addEventListener("DOMContentLoaded", function(event) { 
				//do work
			});
			
			function openSquareClick(elem){
				console.log(elem);
			}
		</script>
	</body>
</html>