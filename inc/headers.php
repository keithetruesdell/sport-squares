<?php 

$headerContents = file_get_contents(__SITE_PATH."/data/headers.json");
$headerArr = json_decode($headerContents, true);

// use the colNames if there is as many as the defaults
$colHeaders = $headerArr["colsDefaults"];
if (count($headerArr["colsNames"]) == count($headerArr["colsDefaults"])) {
	$colHeaders = $headerArr["colsNames"];
}

// use the rowsNames if there is as many as the defaults
$rowHeaders = $headerArr["rowsDefaults"];
if (count($headerArr["rowsNames"]) == count($headerArr["rowsDefaults"])) {
	$rowHeaders = $headerArr["rowsNames"];
}


// create a row of column headers
// first - create blank cell, then the named headers
$topRow = "<div class='row'><div class='col'></div>";
foreach ($colHeaders AS $colName) {
	$topRow .= "<div class='col bg-grey bolder upper'><span class=''>".trim($colName)."</span></div>";
}
$topRow .= "</div>";  // closes row div
