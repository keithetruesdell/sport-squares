<?php
	// define the site path  
	$site_path = realpath(dirname(__FILE__));
	define ("__SITE_PATH", $site_path);
	
	// get config options  
	include_once(__SITE_PATH."/inc/config.php");

	// set the default timezone based on config  
	date_default_timezone_set ($config["dateDefaultTimezone"]);
	
	// create html doc head data variable  
	include_once(__SITE_PATH."/inc/headData.php");
	
	// get header options and contents  
	include_once(__SITE_PATH."/inc/headers.php");

	// get square contents  
	include_once(__SITE_PATH."/inc/squares.php");
	
	echo $headData;
	// NOTE: add messaging about squares not drawn yet and eveyrthing is just a placeholder, plus the date/time for the number drawing.
	echo $topRow;
	echo $squaresOut;
	
?>