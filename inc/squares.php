<?php 

$squaresContents = file_get_contents(__SITE_PATH."/data/squares.json");
$squares = json_decode($squaresContents, true);

//
$squaresData = $squares["data"];
$squaresOut = "";
foreach ($squaresData AS $rowIdx=>$squaresRow) {
	$squaresOut .= "<div class='row'><div class='col bg-grey bolder upper'>".trim($rowHeaders[$rowIdx])."</div>";
	foreach ($squaresRow AS $colIdx=>$square) {
		$squareID = $colIdx.$rowIdx;
		$squareNm = $colHeaders[$colIdx].$rowHeaders[$rowIdx];
		$sqr = "";
		$SQDN = trim($square['displayName']);
		if (strlen($SQDN) > 1) {
			$sqrDN = "";
			if ($config['showNames']) {
				$sqrDN = $SQDN;
			} elseif (strlen($SQDN) > 0 && $square['paid']) {
				$sqrDN = "PAID";
			} else {
				$sqrDN = "RESERVED";
			}
			$sqr = "<span class='bg-grey' data-sqid='".$squareID."'>$sqrDN</span>";
		} else {
			$sqr = "<button type='button' class='btn btn-opensquare' data-colid='".$colHeaders[$colIdx]."' data-rowid='".$rowHeaders[$rowIdx]."' data-square='".$squareNm."' data-sqid='".$squareID."' data-toggle='modal' data-target='#requestModal'>OPEN</button>";
		}
		$squaresOut .= "<div class='col btnClick' >$sqr</div>";
	}
	$squaresOut .= "</div>"; // end row
}
