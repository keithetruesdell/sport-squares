<?php 
$headData = '
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta content="en-us" http-equiv="Content-Language" />
		<meta content="text/html; charset=windows-1252" http-equiv="Content-Type" />
		
		<meta name="Author" content="" />
		
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<meta name="Language" content="English" />
		<meta name="Designer" content="" />
		<meta name="Publisher" content="" />
		<meta name="Revisit-After" content="1 days Days" />
		<meta name="Distribution" content="Global">
		<meta name="Robots" content="all" />
		<base target="_self" />
		
		<meta name="title" content="'.$config['title'].'" />
		<meta name="Subject" content="'.$config['title'].'" />
		<title>'.$config['title'].'</title>
		
		<!-- Shortcut Icon -->
		<link rel="icon" type="image/x-icon" href="/favicon.ico" />
		<link rel="shortcut icon" href="/favicon.ico" />
		<link rel="apple-touch-icon" href="/apple-touch-icon.png" />
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="/assets/css/bootstrap/4.5.0/bootstrap.min.css" crossorigin="anonymous">
		
		<!-- Font Awesome Free -->
		<link rel="stylesheet" href="/assets/css/fontawesome/5.13/all.min.css" />
		
		<!-- Google fonts -->
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
		<link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
		<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
		
		
		<!-- load theme css files -->
		<link rel="stylesheet" href="/assets/css/bootstrap/custom.css" />
		<style>
			main, #main, .main { margin: 15px; }
			.squares-container { border-width: .2rem; border: solid #000000; }
			.row { margin-right: 0px; margin-left: 0px; }
			.row>.col { border: 1px solid #000000; padding-top: 1rem; padding-bottom: 1rem; }
		
			.bg-grey { background-color: #c7c7c7; }
			
			.bolder { font-weight: bolder; }
			.ucase, .upper { text-transform: capitalize; }
			
			.btnClick>a { cursor: pointer; }
		</style>
	</head>
	';